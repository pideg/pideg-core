/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#include <stdio.h>
#include "plot.h"

void plot(int size, float *data)
{
    // open persistent gnuplot window
    FILE *gnuplot_pipe = popen("gnuplot -persistent", "w");
    // basic settings
    fprintf(gnuplot_pipe, "set title '%s'\n", "PID Envelope");
    // fill it with data
    fprintf(gnuplot_pipe, "plot '-'\n");
    for (int i = 0; i < size; i++)
    {
        fprintf(gnuplot_pipe, "%d %f\n", i, data[i]);
    }

    fprintf(gnuplot_pipe, "e\n");

    // refresh can probably be omitted
    fprintf(gnuplot_pipe, "refresh\n");
}
