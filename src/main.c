/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#include <stdio.h>
#include "pideg.h"
#include "constants.h"
#include "plot.h"

int main(void)
{
    long perPhaseSimulationSamples = SAMPLING_RATE * 1;
    float pidegData[perPhaseSimulationSamples * 2];

    PIDEG envelope;
    PIDEGInitialize(&envelope);

    char envelopeName[] = "Trial Envelope\n";
    envelope.parameters[0] = 0.001;
    envelope.parameters[1] = 0;
    envelope.parameters[2] = 0;
    envelope.parameters[3] = 50000;
    envelope.parameters[4] = 50000;

    PIDEGKeyOn(&envelope);
    for (int i = 1; i <= perPhaseSimulationSamples; i++)
    {
        pidegData[i] = PIDEGGenerateSample(&envelope);
    }

    PIDEGKeyOff(&envelope);
    for (int i = perPhaseSimulationSamples; i <= 2 * perPhaseSimulationSamples; i++)
    {
        pidegData[i] = PIDEGGenerateSample(&envelope);
    }

    printf("%s", envelopeName);
    for (int i = 0; i < 2 * perPhaseSimulationSamples; i++)
    {
        printf("%f\n", pidegData[i]);
    }

    plot(2 * perPhaseSimulationSamples, pidegData);

    return 0;
}