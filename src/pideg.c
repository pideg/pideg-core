/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include "pideg.h"

void PIDEGInitialize(PIDEG *pideg)
{
    {
        pideg->parameters[0] = 0.0001f;
        pideg->parameters[1] = 0;
        pideg->parameters[2] = 0;

        pideg->parameters[3] = 1.0f;
        pideg->parameters[4] = 1.0f;

        pideg->parameters[5] = 1.0f;
    }

    pideg->previousError = 0;
    pideg->integral = 0;

    pideg->setPoint = 1.0f;
    pideg->measuredValue = 0;

    pideg->sampleCount = 0;
    pideg->pidegMode = 1;
    pideg->changeOverValue = 1.0f;
}

void PIDEGKeyOn(PIDEG *pideg)
{
    pideg->pidegMode = 1;
    pideg->sampleCount = 0;
}

void PIDEGKeyOff(PIDEG *pideg)
{
    pideg->pidegMode = -1;
    pideg->sampleCount = 0;
}

float PIDEGGenerateSample(PIDEG *pideg)
{
    float *tempParameters = pideg->parameters;
    float value;

    if (pideg->pidegMode == 1 && pideg->sampleCount % 2 == 0)
    {
        pideg->setPoint = 1 - exp(-tempParameters[3] * pideg->sampleCount / SAMPLING_RATE);
    }
    else if (pideg->pidegMode == -1)
    {
        pideg->changeOverValue = pideg->setPoint;
        pideg->pidegMode = 0;
    }
    else if (pideg->pidegMode == 0 && pideg->sampleCount % 2 == 0)
    {
        pideg->setPoint = pideg->changeOverValue * exp(-tempParameters[4] * pideg->sampleCount / SAMPLING_RATE);
    }

    float error = pideg->setPoint - pideg->measuredValue;
    pideg->integral += error;
    // float output = tempParameters[0] * error + tempParameters[1] *pideg->integral + tempParameters[2] * (error -pideg->previousError) * 10;
    float output = tempParameters[0] * error + tempParameters[1] * pideg->integral + tempParameters[2] * (error - pideg->previousError);
    // pideg->measuredValue += output;
    pideg->measuredValue = MAX(0.0, MIN(1.0, pideg->measuredValue + output));

    pideg->previousError = error;
    pideg->sampleCount++;

    return pideg->measuredValue;
}