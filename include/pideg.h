/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#ifndef PIDEG_H
#define PIDEG_H

#include "constants.h"
typedef struct PIDEGnvelope
{
    float parameters[TOTAL_PARAMETERS];

    float previousError;
    float integral;

    float setPoint;
    float measuredValue;

    int sampleCount;
    int pidegMode;
    float changeOverValue;
} PIDEG;

extern void PIDEGInitialize(PIDEG *pideg);

extern void PIDEGKeyOn(PIDEG *pideg);

extern void PIDEGKeyOff(PIDEG *pideg);

extern float PIDEGGenerateSample(PIDEG *pideg);

#endif