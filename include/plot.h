/*
"SPDX-License-Identifier: MPL-2.0
Copyright © 2021 Ashwin Pillay [fname][lname]@protonmail[dot]com"
*/

#ifndef PLOT_H
#define PLOT_H

#include <stdint.h>

extern void plot(int size, float *data);

#endif