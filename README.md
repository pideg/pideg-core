# PID Envelope Generator Core Library

The PID Envelope Generator (PIDEG) Core is a C based library that can be used to integrate the functionality of the [PID Envelope Generator (PIDEG)](https://arxiv.org/abs/2106.13966) specification in other applications.

The PIDEG core implements the fundamental envelope generation process, providing abstraction layers to:

- Initialize the generator
- Accept `KEY_ON `and `KEY_OFF` messages
- Request the next envelope sample while internally maintaining the execution context

The library is intended to be used for:

- VST/LV2 implementations
- Adding PIDEG support into existing audio libraries, toolkits & frameworks
- Incorporating PIDEG functionalities in any other desired applications targetted for desktops and mobiles

This project additionally contains a demo usage of the PIDEG core; defining the generation of a sample envelope of 2000 samples *(with `KEY_ON` applied at N=0 and `KEY_OFF` applied at N=1000)* and plotting it through `gnuplot`.

<p align="center">
  <img src="docs/demo_screenshot.png" />
</p>

## Installation

Linux:

1. Clone this repo to local, switch to repo

```sh
git clone https://gitlab.com/pid-envelope/pideg-core.git

cd pideg-core
```

2. Generate the binary by using the `Makefile`

```sh
make
```

## Usage

### Prerequisite

[gnuplot](http://www.gnuplot.info/) must be installed in the system and available via CLI. Install it using your favourite package manager.

### Evaluating the library

#### Linux

Running the binary

```sh
./bin/main
```

To execute any changes made to the source code, rebuild the binary using `Makefile` before running it.

### Using the library in another project

While not mandatory, a typical placement of this library in another project `foo` maybe as follows:

```text
foo
|--bin
    |- *foo binaries here*
|--include
    |- *foo header files here*
|--lib
|  |--pideg-core
|  |  |--src
|  |     |- pideg.c
|  |     |- pideg.h
|--src
   |- *foo source files here*
```

To ensure builds are successful, the `Makefile` for `foo` needs to be written according to the project structure used.

Then, the library can be used in any source/header file of `foo` as follows:

```C
#include "pideg.h"
```

## Contributing

0. [Report Bugs or Request Features](https://gitlab.com/pid-envelope/pideg-core/-/issues)
1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Merge/Pull Request

## License

Distributed under the Mozilla Public License Version 2.0 License. See `LICENSE` for more information.
